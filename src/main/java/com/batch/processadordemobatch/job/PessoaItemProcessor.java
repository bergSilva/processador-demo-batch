package com.batch.processadordemobatch.job;

import com.batch.processadordemobatch.model.Pessoa;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class PessoaItemProcessor implements ItemProcessor<Pessoa, Pessoa> {
    private static final Logger log = LoggerFactory.getLogger(PessoaItemProcessor.class);

    @Override
    public Pessoa process(Pessoa pessoa) throws Exception {
        final String nome = pessoa.getNome().toUpperCase();
        final String sobreNome = pessoa.getSobreNome().toUpperCase();

        final Pessoa objetoPessoaTrasnformado = new Pessoa(nome, sobreNome);

        log.info("Converting (" + pessoa + ") into (" + objetoPessoaTrasnformado + ")");
        return objetoPessoaTrasnformado;
    }
}
